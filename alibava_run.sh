#./run_analysis \
#--pedestals input/Ped_-400_THOR_170920_1711 \
#--calibration input/CalibGain_-400_THOR_170920_1711 \
#--tdc-low 0 \
#--tdc-high 50 \
#--seed_cut 5 \
#--neigh_cut 3 \
#--out RS_-400_HADIS_180920_5417_time_window.root \
#input/RS_-400_HADIS_180920_5417


./run_analysis \
--pedestals input/LaserRun_20201110/Ped.h5 \
--calibration input/LaserRun_20201110/CaliGain.h5 \
--tdc-low 0 \
--tdc-high 1000 \
--seed_cut 5 \
--neigh_cut 3 \
--out LR_10.root \
input/LaserRun_20201110/LR_10.h5
