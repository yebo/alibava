import os, sys, subprocess

DataDir = 'input/LaserRun_20201110'
PedFile = 'input/LaserRun_20201110/Ped'
CaliFile = 'input/LaserRun_20201110/CaliGain'
files=os.listdir(DataDir)
files.sort()
for file in files:
    if ".h5" not in file: continue
    data = DataDir + "/" + file
    pos = file.split(".")[0]
    cmd = './run_analysis' \
        + ' --pedestals ' + PedFile \
        + ' --calibration ' + CaliFile \
        + ' --tdc-low 0' \
        + ' --tdc-high 1000' \
        + ' --seed_cut 5' \
        + ' --neigh_cut 3' \
        + ' --out ' + pos + '.root ' \
        + data
    subprocess.call(cmd, shell=True)

#DataDir = 'input/CentralPosition'
#PedFile = 'input/CentralPosition/Ped_210V.ped'
#CaliFile = 'input/CentralPosition/CalGain_210.cal'
#files=os.listdir(DataDir)
#files.sort()
#for file in files:
#    if ".data" not in file: continue
#    data = DataDir + "/" + file
#    pos = file.split(".")[0]
#    cmd = './run_analysis' \
#        + ' --pedestals ' + PedFile \
#        + ' --calibration ' + CaliFile \
#        + ' --tdc-low 10' \
#        + ' --tdc-high 20' \
#        + ' --seed_cut 5' \
#        + ' --neigh_cut 3' \
#        + ' --out Central_' + pos + '.root ' \
#        + data
#    subprocess.call(cmd, shell=True)
